package com.epam.view;

import com.epam.Application;

import com.epam.model.flowersalons.bouquetImp.*;
import com.epam.model.flowersalons.decorator.BouquetDecorator;
import com.epam.model.flowersalons.factory.*;
import com.epam.model.flowersalons.observer.ClientsInfo;
import com.epam.model.flowersalons.observer.SalonInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * Class to connect with controller, this class just show the data.
 *
 * @version 2.1
 * Created by Borys Latyk on 13/11/2019.
 * @since 10.11.2019
 */
public class View implements CatalogOrders, Composition, EventsMenu {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));


    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", ("1 - Order from catalog"));
        menu.put("2", ("2 - Bouquets for events"));
        menu.put("Q", ("Q - Exit"));
    }

    public View() {
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("Q", this::pressButton3);


    }

    private void pressButton1() throws IOException {
        View view = new View();
        String catalog;
        do {
            view.showCatalog();
            catalog = input.readLine();
            if (catalog.equals("1")) {
                BouquetDecorator.i = 0.0;
                AmethystBouquet a1 = new AmethystBouquet();
                logger1.info("Price " + a1.getCosts());
                logger1.info(a1.getName() + " " + a1.getFlowersAndRibonns());
                view.showComposition(a1);
                SalonInfo salonInfo = new SalonInfo();
                ClientsInfo clientsInfo = new ClientsInfo();
                salonInfo.addObserver(clientsInfo);
                salonInfo.setInfo("Order is ready,confirm the mail for transportation");
            } else if (catalog.equals("2")) {
                BouquetDecorator.i = 0.0;
                CelebrationRose a1 = new CelebrationRose();
                logger1.info("Price " + a1.getCosts());
                logger1.info(a1.getName() + " " + a1.getFlowersAndRibonns());
                view.showComposition(a1);
                SalonInfo salonInfo = new SalonInfo();
                ClientsInfo clientsInfo = new ClientsInfo();
                salonInfo.addObserver(clientsInfo);
                salonInfo.setInfo("Order is ready,confirm the mail for transportation");
            } else if (catalog.equals("3")) {
                BouquetDecorator.i = 0.0;
                FestiveLily a1 = new FestiveLily();
                logger1.info("Price " + a1.getCosts());
                logger1.info(a1.getName() + " " + a1.getFlowersAndRibonns());
                view.showComposition(a1);
                SalonInfo salonInfo = new SalonInfo();
                ClientsInfo clientsInfo = new ClientsInfo();
                salonInfo.addObserver(clientsInfo);
                salonInfo.setInfo("Order is ready,confirm the mail for transportation");
            } else if (catalog.equals("4")) {
                BouquetDecorator.i = 0.0;
                WhiteLilacBouquet a1 = new WhiteLilacBouquet();
                logger1.info("Price " + a1.getCosts());
                logger1.info(a1.getName() + " " + a1.getFlowersAndRibonns());
                view.showComposition(a1);
                SalonInfo salonInfo = new SalonInfo();
                ClientsInfo clientsInfo = new ClientsInfo();
                salonInfo.addObserver(clientsInfo);
                salonInfo.setInfo("Order is ready,confirm the mail for transportation");
            } else if (catalog.equals("5")) {
                BouquetDecorator.i = 0.0;
                WhiteMedleyBouquet a1 = new WhiteMedleyBouquet();
                logger1.info("Price " + a1.getCosts());
                logger1.info(a1.getName() + " " + a1.getFlowersAndRibonns());
                view.showComposition(a1);
                SalonInfo salonInfo = new SalonInfo();
                ClientsInfo clientsInfo = new ClientsInfo();
                salonInfo.addObserver(clientsInfo);
                salonInfo.setInfo("Order is ready,confirm the mail for transportation");
            }

        } while (!catalog.equalsIgnoreCase("e"));

    }

    private void pressButton2() throws IOException {
        View view2 = new View();
        String events;
        MainEvents mainEvents;

        do {
            view2.showEvents();
            events = input.readLine();
            if (events.equals("1")) {
                mainEvents = new Wedding();
                mainEvents.prepareBouquet(BouquetsForEvents.WeddingBouquet);
                SalonInfo salonInfo = new SalonInfo();
                ClientsInfo clientsInfo = new ClientsInfo();
                salonInfo.addObserver(clientsInfo);
                salonInfo.setInfo("Confirm the mail for transportation");
            } else if (events.equals("2")) {
                mainEvents = new Birthday();
                mainEvents.prepareBouquet(BouquetsForEvents.BirthdaysBouquets);
                SalonInfo salonInfo = new SalonInfo();
                ClientsInfo clientsInfo = new ClientsInfo();
                salonInfo.addObserver(clientsInfo);
                salonInfo.setInfo("Confirm the mail for transportation");
            } else if (events.equals("3")) {
                mainEvents = new ValentinesDay();
                mainEvents.prepareBouquet(BouquetsForEvents.ValentinesDayBouquets);
                SalonInfo salonInfo = new SalonInfo();
                ClientsInfo clientsInfo = new ClientsInfo();
                salonInfo.addObserver(clientsInfo);
                salonInfo.setInfo("Confirm the mail for transportation");
            } else if (events.equals("4")) {
                mainEvents = new Funeral();
                mainEvents.prepareBouquet(BouquetsForEvents.FuneralBouquets);
                SalonInfo salonInfo = new SalonInfo();
                ClientsInfo clientsInfo = new ClientsInfo();
                salonInfo.addObserver(clientsInfo);
                salonInfo.setInfo("Confirm the mail for transportation");

            }
        } while (!events.equalsIgnoreCase("e"));
    }


    private void pressButton3() {
        logger1.info("Bye-Bye");
    }


    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger1.info("\nFlower Salon:");
        for (String str : menu.values()) {
            logger1.info(str);
        }
    }

    public void show() throws Exception {
        String keyMenu;
        do {
            outputMenu();
            logger1.info("Please, select menu point");
            keyMenu = input.readLine().toUpperCase();
            methodsMenu.get(keyMenu).print();
        } while (!keyMenu.equals("Q"));
    }
}


