package com.epam.view;

import com.epam.Application;
import com.epam.model.flowersalons.Bouquet;
import com.epam.model.flowersalons.decorator.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Interface to show menu with decorations.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public interface Composition {
    BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));
    Logger logger1 = LogManager.getLogger(Application.class);


    default void showComposition(Composition obj) throws IOException {
        List<String> composition;
        Double newPrice;
        String name;
        String compsition;

        do {
            logger1.info("Add decoration");
            logger1.info("1 - Black Ribbon");
            logger1.info("2 - Correlative");
            logger1.info("3 - Felt");
            logger1.info("4 - Gift Bag");
            logger1.info("5 - Rattan");
            logger1.info("6 - White Ribbon");
            logger1.info("Exit - press 'e' ");
            compsition = input.readLine();
            if (compsition.equalsIgnoreCase("1")) {
                BouquetDecorator b1 = new BlackRibbon();
                b1.setBouquetOptional((Bouquet) obj);
                newPrice = b1.getCosts() + BouquetDecorator.i;
                name = b1.getName();
                composition = b1.getFlowersAndRibonns();

                logger1.info("Price " + newPrice);
                logger1.info("Item " + name);
                logger1.info("Bouquet components " + composition);

            } else if (compsition.equalsIgnoreCase("2")) {
                BouquetDecorator b2 = new Correlative();
                b2.setBouquetOptional((Bouquet) obj);
                newPrice = b2.getCosts() + BouquetDecorator.i;
                name = b2.getName();
                composition = b2.getFlowersAndRibonns();
                logger1.info("Price " + newPrice);
                logger1.info("Item " + name);
                logger1.info("Bouquet components " + composition);

            } else if (compsition.equalsIgnoreCase("3")) {
                BouquetDecorator b1 = new Felt();
                b1.setBouquetOptional((Bouquet) obj);
                newPrice = b1.getCosts() + BouquetDecorator.i;
                name = b1.getName();
                composition = b1.getFlowersAndRibonns();
                logger1.info("Price " + newPrice);
                logger1.info("Item " + name);
                logger1.info("Bouquet components " + composition);

            } else if (compsition.equalsIgnoreCase("4")) {
                BouquetDecorator b1 = new GiftBag();
                b1.setBouquetOptional((Bouquet) obj);
                newPrice = b1.getCosts() + BouquetDecorator.i;
                name = b1.getName();
                composition = b1.getFlowersAndRibonns();
                logger1.info("Price " + newPrice);
                logger1.info("Item " + name);
                logger1.info("Bouquet components " + composition);

            } else if (compsition.equalsIgnoreCase("5")) {
                BouquetDecorator b1 = new Rattan();
                b1.setBouquetOptional((Bouquet) obj);
                newPrice = b1.getCosts() + BouquetDecorator.i;
                name = b1.getName();
                composition = b1.getFlowersAndRibonns();
                logger1.info("Price " + newPrice);
                logger1.info("Item " + name);
                logger1.info("Bouquet components " + composition);

            } else if (compsition.equalsIgnoreCase("6")) {
                BouquetDecorator b1 = new WhiteRibbon();
                b1.setBouquetOptional((Bouquet) obj);
                newPrice = b1.getCosts() + BouquetDecorator.i;
                name = b1.getName();
                composition = b1.getFlowersAndRibonns();
                logger1.info("Price " + newPrice);
                logger1.info("Item " + name);
                logger1.info("Bouquet components " + composition);

            }
        } while (!compsition.equalsIgnoreCase("e"));
    }
}