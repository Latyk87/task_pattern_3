package com.epam.view;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Interface to show catalog menu.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public interface CatalogOrders {
    Logger logger1 = LogManager.getLogger(Application.class);

    default void showCatalog() {
        logger1.info("Choose the Bouquet");
        logger1.info("1 - Amethyst Bouquet");
        logger1.info("2 - Celebration Rose");
        logger1.info("3 - Festive Lily");
        logger1.info("4 - White Lilac Bouquet");
        logger1.info("5 - White Medley Bouquet");
        logger1.info("Exit - press 'e' ");
    }
}