package com.epam.model;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
/**
 * Class sends the sms for fatal problems in following program.
 * Created by Borys Latyk on 21/11/2019.
 * @version 2.1
 * @since 21.11.2019
 */
public class ExampleSMS {
    public static final String ACCOUNT_SID ="AC8a68c92d05d6ef219c55582dc407f6f3";
    public static final String AUTH_TOKEN ="5b3d5a48e48e407392b3e7f1e2c8e7c0";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380676726762"),
                        new PhoneNumber("+19386665331"), str) .create();
    }
}
