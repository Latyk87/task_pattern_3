package com.epam.model.flowersalons.factory;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
/**
 * Class  produces instances for Valentines day bouquets.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class ValentinesDay extends MainEvents implements Sellect {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));



    @Override
    protected Events createEvent(BouquetsForEvents bouquets) throws IOException {
        Events events1 = null;
        ValentinesDay valentinesDay = new ValentinesDay();
        List<ValentinesDayBouquets> valentinesDayBouquets = new ArrayList<>();

        if (bouquets == BouquetsForEvents.ValentinesDayBouquets) {
            ValentinesDayBouquets v1=new ValentinesDayBouquets(3,50,"15 x Red Roses");
            ValentinesDayBouquets v2=new ValentinesDayBouquets(2,70,
                    "9 x Red Roses","10 x Pink Roses");
            ValentinesDayBouquets v3=new ValentinesDayBouquets(1,90,
                    "10 x Pink Roses","11 x Red Roses",true);

            valentinesDayBouquets.add(v1);
            valentinesDayBouquets.add(v2);
            valentinesDayBouquets.add(v3);
            Comparator<ValentinesDayBouquets> bouquetsComparator = ((o1, o2) -> o1.getPosition() - o2.getPosition());
            Collections.sort(valentinesDayBouquets, bouquetsComparator);
            for (ValentinesDayBouquets w : valentinesDayBouquets) {
                logger1.info(w);
            }
            String position;
            int index;
            do {
                valentinesDay.chose();
                position = input.readLine();
                if (position.equals("1")) {
                    index = Integer.parseInt(position) - 1;
                    events1 = valentinesDayBouquets.get(index);
                    logger1.info("Confirm the order by pressing 'e'");
                }
                if (position.equals("2")) {
                    index = Integer.parseInt(position) - 1;
                    events1 = valentinesDayBouquets.get(index);
                    logger1.info("Confirm the order by pressing 'e'");
                }
                if (position.equals("3")) {
                    index = Integer.parseInt(position) - 1;
                    events1 = valentinesDayBouquets.get(index);
                    logger1.info("Confirm the order by pressing 'e'");
                }

            } while (!position.equalsIgnoreCase("e"));

        }

        return events1;


    }
}
