package com.epam.model.flowersalons.factory;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * Interface  shows the menu in factory pattern.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public interface EventsMenu {
    Logger logger1 = LogManager.getLogger(Application.class);

    default void showEvents() {
        logger1.info("Choose the Event");
        logger1.info("1 - Weddings");
        logger1.info("2 - Birthdays");
        logger1.info("3 - Valentines day");
        logger1.info("4 - Funeral");
        logger1.info("Exit - press 'e' ");
    }
}
