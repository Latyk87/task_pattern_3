package com.epam.model.flowersalons.factory;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
/**
 * Class  produces instances for Funeral bouquets.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class Funeral extends MainEvents implements Sellect {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));


    @Override
    protected Events createEvent(BouquetsForEvents bouquets) throws IOException {
        Events events1 = null;
        Funeral funeral = new Funeral();
        List<FuneralBouquets> funeralBouquets = new ArrayList<>();

        if (bouquets == BouquetsForEvents.FuneralBouquets) {
            FuneralBouquets f1=new FuneralBouquets(3,40,
                    "10 x Red Roses",4);
            FuneralBouquets f2=new FuneralBouquets(1,55,
                    "12 x Red Roses",true, 4);
            FuneralBouquets f3=new FuneralBouquets(2,65,
                    "16 x Red Roses",true,6);

           funeralBouquets.add(f1);
            funeralBouquets.add(f2);
            funeralBouquets.add(f3);
            Comparator<FuneralBouquets> bouquetsComparator = ((o1, o2) -> o1.getPosition() - o2.getPosition());
            Collections.sort(funeralBouquets, bouquetsComparator);
            for (FuneralBouquets w :funeralBouquets ) {
                logger1.info(w);
            }
            String position;
            int index;
            do {
                funeral.chose();
                position = input.readLine();
                if (position.equals("1")) {
                    index = Integer.parseInt(position) - 1;
                    events1 = funeralBouquets.get(index);
                    logger1.info("Confirm the order by pressing 'e'");
                }
                if (position.equals("2")) {
                    index = Integer.parseInt(position) - 1;
                    events1 = funeralBouquets.get(index);
                    logger1.info("Confirm the order by pressing 'e'");
                }
                if (position.equals("3")) {
                    index = Integer.parseInt(position) - 1;
                    events1 = funeralBouquets.get(index);
                    logger1.info("Confirm the order by pressing 'e'");
                }

            } while (!position.equalsIgnoreCase("e"));

        }

        return events1;
    }
}
