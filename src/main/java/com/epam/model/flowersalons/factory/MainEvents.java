package com.epam.model.flowersalons.factory;

import java.io.IOException;


public abstract class MainEvents {
    protected abstract Events createEvent(BouquetsForEvents bouquets) throws IOException;

    public Events prepareBouquet(BouquetsForEvents bouquetsForEvents) throws IOException {

        Events bouquetsReady = createEvent(bouquetsForEvents);
        bouquetsReady.createSet();

    return bouquetsReady;
    }
}