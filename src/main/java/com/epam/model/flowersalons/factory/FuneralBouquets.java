package com.epam.model.flowersalons.factory;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Objects;
/**
 * Class  describes and implements Funeral bouquets.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class FuneralBouquets implements Events {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private int position;
    private int price;
    private String flowersSet1;
    private boolean blackBag;
    private int blackRibbon;

    public FuneralBouquets(int position, int price, String flowersSet1, int blackRibbon) {
        this.position = position;
        this.price = price;
        this.flowersSet1 = flowersSet1;
        this.blackRibbon = blackRibbon;
    }

    public FuneralBouquets(int position, int price, String flowersSet1, boolean blackBag, int blackRibbon) {
        this.position = position;
        this.price = price;
        this.flowersSet1 = flowersSet1;
        this.blackBag = blackBag;
        this.blackRibbon = blackRibbon;
    }

    public int getPosition() {
        return position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FuneralBouquets)) return false;
        FuneralBouquets that = (FuneralBouquets) o;
        return position == that.position;
    }

    @Override
    public int hashCode() {
        return Objects.hash(position);
    }

    @Override
    public void createSet() {
        logger1.info("Your order its ready for delivery");
        logger1.info(this);
    }

    @Override
    public String toString() {
        return "FuneralBouquets{" +
                "position=" + position +
                ", price=" + price +
                ", flowersSet1='" + flowersSet1 + '\'' +
                ", blackBag=" + blackBag +
                ", blackRibbon=" + blackRibbon +
                '}';
    }
}
