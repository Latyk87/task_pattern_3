package com.epam.model.flowersalons.factory;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
/**
 * Class  produces instances for Birthday bouquets.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class Birthday extends MainEvents implements Sellect {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));

    @Override
    protected Events createEvent(BouquetsForEvents bouquets) throws IOException {
        Events events1 = null;
        Birthday birthday = new Birthday();
        List<BirthdaysBouquets> birthdaysBouquets = new ArrayList<>();

        if (bouquets == BouquetsForEvents.BirthdaysBouquets) {
            BirthdaysBouquets b1 = new BirthdaysBouquets(1, 40, "3 x Cerise Rose", true);
            BirthdaysBouquets b2 = new BirthdaysBouquets(3, 65, "16 x Pink Roses",
                    "3 x Burgundy Hypericum", true);
            BirthdaysBouquets b3 = new BirthdaysBouquets(2, 80, "5 x White Roses",
                    "8 x Lilac Spray Roses", true, 3);

            birthdaysBouquets.add(b1);
            birthdaysBouquets.add(b2);
            birthdaysBouquets.add(b3);
            Comparator<BirthdaysBouquets> bouquetsComparator = ((o1, o2) -> o1.getPosition() - o2.getPosition());
            Collections.sort(birthdaysBouquets, bouquetsComparator);
            for (BirthdaysBouquets w : birthdaysBouquets) {
                logger1.info(w);
            }
            String position;
            int index;
            do {
                birthday.chose();
                position = input.readLine();
                if (position.equals("1")) {
                    index = Integer.parseInt(position) - 1;
                    events1 = birthdaysBouquets.get(index);
                    logger1.info("Confirm the order by pressing 'e'");
                }
                if (position.equals("2")) {
                    index = Integer.parseInt(position) - 1;
                    events1 = birthdaysBouquets.get(index);
                    logger1.info("Confirm the order by pressing 'e'");
                }
                if (position.equals("3")) {
                    index = Integer.parseInt(position) - 1;
                    events1 = birthdaysBouquets.get(index);
                    logger1.info("Confirm the order by pressing 'e'");
                }

            } while (!position.equalsIgnoreCase("e"));

        }

        return events1;
    }
}
