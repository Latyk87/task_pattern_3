package com.epam.model.flowersalons.factory;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Objects;

/**
 * Class describes and implements Valentines day bouquets.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class ValentinesDayBouquets implements Events {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private int position;
    private int price;
    private String flowersSet1;
    private String flowersSet2;
    private boolean redGiftbag;

    public ValentinesDayBouquets(int position, int price, String flowersSet1) {
        this.position = position;
        this.price = price;
        this.flowersSet1 = flowersSet1;
    }

    public ValentinesDayBouquets(int position, int price, String flowersSet1, String flowersSet2) {
        this.position = position;
        this.price = price;
        this.flowersSet1 = flowersSet1;
        this.flowersSet2 = flowersSet2;
    }

    public ValentinesDayBouquets(int position, int price, String flowersSet1, String flowersSet2, boolean redGiftbag) {
        this.position = position;
        this.price = price;
        this.flowersSet1 = flowersSet1;
        this.flowersSet2 = flowersSet2;
        this.redGiftbag = redGiftbag;
    }

    public int getPosition() {
        return position;
    }

    @Override
    public void createSet() {
        logger1.info("Your order its ready for delivery");
        logger1.info(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ValentinesDayBouquets)) return false;
        ValentinesDayBouquets that = (ValentinesDayBouquets) o;
        return position == that.position;
    }

    @Override
    public int hashCode() {
        return Objects.hash(position);
    }

    @Override
    public String toString() {
        return "ValentinesDayBouquets{" +
                "position=" + position +
                ", price=" + price +
                ", flowersSet1='" + flowersSet1 + '\'' +
                ", flowersSet2='" + flowersSet2 + '\'' +
                ", redGiftbag=" + redGiftbag +
                '}';
    }
}
