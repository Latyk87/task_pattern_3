package com.epam.model.flowersalons.factory;
/**
 * Enum contains type of events bouquets.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public enum BouquetsForEvents {
    WeddingBouquet,BirthdaysBouquets,
    ValentinesDayBouquets,FuneralBouquets
}
