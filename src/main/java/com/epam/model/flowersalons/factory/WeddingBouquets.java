package com.epam.model.flowersalons.factory;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Objects;
/**
 * Class  describes and implements Wedding Bouquets.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class WeddingBouquets implements Events {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private int position;
    private int price;
    private String flowersSet1;
    private String flowersSet2;
    private String flowersSet3;
    private String flowersSet4;
    private String flowersSet5;
    private boolean giftBag;


    public WeddingBouquets(int position, int price, String flowersSet1, String flowersSet2, String flowersSet3, String flowersSet4) {
        this.position = position;
        this.price = price;
        this.flowersSet1 = flowersSet1;
        this.flowersSet2 = flowersSet2;
        this.flowersSet3 = flowersSet3;
        this.flowersSet4 = flowersSet4;
    }

    public WeddingBouquets(int position, int price, String flowersSet1, String flowersSet2, String flowersSet3, String flowersSet4, String flowersSet5) {
        this.position = position;
        this.price = price;
        this.flowersSet1 = flowersSet1;
        this.flowersSet2 = flowersSet2;
        this.flowersSet3 = flowersSet3;
        this.flowersSet4 = flowersSet4;
        this.flowersSet5 = flowersSet5;
    }

    public WeddingBouquets(int position, int price, String flowersSet1, String flowersSet2, String flowersSet3, String flowersSet4, String flowersSet5, boolean giftBag) {
        this.position = position;
        this.price = price;
        this.flowersSet1 = flowersSet1;
        this.flowersSet2 = flowersSet2;
        this.flowersSet3 = flowersSet3;
        this.flowersSet4 = flowersSet4;
        this.flowersSet5 = flowersSet5;
        this.giftBag = giftBag;
    }

    public int getPosition() {
        return position;
    }


    @Override
    public void createSet() {
        logger1.info("Your order its ready for delivery");
        logger1.info(this);
    }



    @Override
    public String toString() {
        return "WeddingBouquets{" +
                "position=" + position +
                ", price=" + price +
                ", flowersSet1='" + flowersSet1 + '\'' +
                ", flowersSet2='" + flowersSet2 + '\'' +
                ", flowersSet3='" + flowersSet3 + '\'' +
                ", flowersSet4='" + flowersSet4 + '\'' +
                ", flowersSet5='" + flowersSet5 + '\'' +
                ", giftBag=" + giftBag +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WeddingBouquets)) return false;
        WeddingBouquets that = (WeddingBouquets) o;
        return position == that.position;
    }

    @Override
    public int hashCode() {
        return Objects.hash(position);
    }
}
