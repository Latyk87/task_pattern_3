package com.epam.model.flowersalons.factory;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * Interface  shows the menu in factory pattern.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public interface Sellect {
    Logger logger1 = LogManager.getLogger(Application.class);

    default void chose() {
        logger1.info("Please select a position and press 'e' to confirm the order");
        logger1.info("1");
        logger1.info("2");
        logger1.info("3");
        logger1.info("Confirm - 'e'");
    }
}
