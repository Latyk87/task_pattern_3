package com.epam.model.flowersalons.factory;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Class  produces instances for Wedding Bouquets.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class Wedding extends MainEvents implements Sellect {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));

    @Override
    protected Events createEvent(BouquetsForEvents bouquets) throws IOException {
        Events events1 = null;
        Wedding wedding = new Wedding();
        List<WeddingBouquets> weddingBouquets = new ArrayList<>();

        if (bouquets == BouquetsForEvents.WeddingBouquet) {
            WeddingBouquets w1 = new WeddingBouquets(2, 75, "6 x Premium White Roses",
                    "3 x Purple Stocks",
                    "4 x Lilac Stocks", "3 x Blue Spray Veronica");

            WeddingBouquets w2 = new WeddingBouquets(1, 110, "10 x Pink Sweet Avalanche™ Roses",
                    "6 x Double Pink Lisianthus", "5 x Pink Antirrhinum",
                    "2 x Green Bell", "3 x Blue Spray Veronica");

            WeddingBouquets w3 = new WeddingBouquets(3, 135, "5 x Burgundy Antirrhinums",
                    "2 x Blue Clematis", "2 x Blue Eryngium", "3 x Purple Alstroemeria",
                    "5 x Purple Roses", true);

            weddingBouquets.add(w1);
            weddingBouquets.add(w2);
            weddingBouquets.add(w3);
            Comparator<WeddingBouquets> bouquetsComparator = ((o1, o2) -> o1.getPosition() - o2.getPosition());
            Collections.sort(weddingBouquets, bouquetsComparator);
            for (WeddingBouquets w : weddingBouquets) {
                logger1.info(w);
            }
            String position;
            int index;
            do {
                wedding.chose();
                position = input.readLine();
                if (position.equals("1")) {
                    index = Integer.parseInt(position) - 1;
                    events1 = weddingBouquets.get(index);
                    logger1.info("Confirm the order by pressing 'e'");
                }
                if (position.equals("2")) {
                    index = Integer.parseInt(position) - 1;
                    events1 = weddingBouquets.get(index);
                    logger1.info("Confirm the order by pressing 'e'");
                }
                if (position.equals("3")) {
                    index = Integer.parseInt(position) - 1;
                    events1 = weddingBouquets.get(index);
                    logger1.info("Confirm the order by pressing 'e'");
                }

            } while (!position.equalsIgnoreCase("e"));

        }

        return events1;
    }


}
