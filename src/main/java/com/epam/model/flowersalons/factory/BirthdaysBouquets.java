package com.epam.model.flowersalons.factory;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Objects;
/**
 * Class  describes and implements Birthday bouquets.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class BirthdaysBouquets implements Events {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private int position;
    private int price;
    private String flowersSet1;
    private String flowersSet2;
    private boolean giftBag;
    private int eucaliptus;


    public BirthdaysBouquets(int position, int price, String flowersSet2, boolean giftBag) {
        this.position = position;
        this.price = price;
        this.flowersSet2 = flowersSet2;
        this.giftBag = giftBag;
    }

    public BirthdaysBouquets(int position, int price, String flowersSet1, String flowersSet2, boolean giftBag) {
        this.position = position;
        this.price = price;
        this.flowersSet1 = flowersSet1;
        this.flowersSet2 = flowersSet2;
        this.giftBag = giftBag;
    }

    public BirthdaysBouquets(int position, int price, String flowersSet1, String flowersSet2, boolean giftBag, int eucaliptus) {
        this.position = position;
        this.price = price;
        this.flowersSet1 = flowersSet1;
        this.flowersSet2 = flowersSet2;
        this.giftBag = giftBag;
        this.eucaliptus = eucaliptus;
    }


    public int getPosition() {
        return position;
    }

    @Override
    public void createSet() {
        logger1.info("Your order its ready for delivery");
        logger1.info(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BirthdaysBouquets)) return false;
        BirthdaysBouquets that = (BirthdaysBouquets) o;
        return position == that.position;
    }

    @Override
    public int hashCode() {
        return Objects.hash(position);
    }

    @Override
    public String toString() {
        return "BirthdaysBouquets{" +
                "position=" + position +
                ", price=" + price +
                ", flowersSet1='" + flowersSet1 + '\'' +
                ", flowersSet2='" + flowersSet2 + '\'' +
                ", giftBag=" + giftBag +
                ", eucaliptus=" + eucaliptus +
                '}';
    }
}
