package com.epam.model.flowersalons.bouquetImp;

import com.epam.model.flowersalons.Bouquet;
import com.epam.view.Composition;
import java.util.LinkedList;
import java.util.List;
/**
 * Class  describes Amethyst bouquet in decorator pattern.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class AmethystBouquet  implements Bouquet, Composition {
    final private double PRICE=30.0;
    final private String NAME="Amethyst Bouquet";
    private List<String> composition;

    public AmethystBouquet(){
        composition=new LinkedList<>();
        composition.add("3 x Lilac Bloom Chrysanthemums");
        composition.add("3 x Purple Stocks");
        composition.add("3 x Lilac September Flower");
        composition.add("3 x Purple Germini");
        composition.add("2 x Eucalyptus Robusta");

    }

    @Override
    public double getCosts() {
        return PRICE;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public List<String> getFlowersAndRibonns() {
        return composition;
    }
}
