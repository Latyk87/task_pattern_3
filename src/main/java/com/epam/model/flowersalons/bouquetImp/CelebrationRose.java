package com.epam.model.flowersalons.bouquetImp;

import com.epam.model.flowersalons.Bouquet;
import com.epam.view.Composition;
import java.util.LinkedList;
import java.util.List;
/**
 * Class  describes Celebration Rose in decorator pattern.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class CelebrationRose  implements Bouquet, Composition {
    final private double PRICE=110.0;
    final private String NAME="Celebration Rose";
    private List<String> composition;

    public CelebrationRose(){
        composition=new LinkedList<>();
        composition.add("5 x Yellow Rose");
        composition.add("3 x Yellow Alstroemeria");
        composition.add("5 x Cerise Rose");
        composition.add("5 x Orange Rose");
        composition.add("2 x Rhododendron");
        composition.add("3 x Cordyline Red Compacta");

    }


    @Override
    public double getCosts() {
        return PRICE;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public List<String> getFlowersAndRibonns() {
        return composition;
    }
}
