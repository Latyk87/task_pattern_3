package com.epam.model.flowersalons.bouquetImp;

import com.epam.model.flowersalons.Bouquet;
import com.epam.view.Composition;
import java.util.LinkedList;
import java.util.List;
/**
 * Class  describes WhiteLilac Bouquet in decorator pattern.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class WhiteLilacBouquet implements Bouquet, Composition {
    final private double PRICE=65.0;
    final private String NAME="White & Lilac Bouquet";
    private List<String> composition;


    public WhiteLilacBouquet(){
        composition=new LinkedList<>();
        composition.add("5 x Lilac Spray Roses");
        composition.add("5 x White Roses");
        composition.add("3 x White Lisianthus");
        composition.add("5 x White Stocks");
        composition.add("3 x Ammi Majus");
        composition.add("6 x Eucalyptus");
        composition.add("3 x Fountain Grass");

    }

    @Override
    public double getCosts() {
        return PRICE;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public List<String> getFlowersAndRibonns() {
        return composition;
    }
}
