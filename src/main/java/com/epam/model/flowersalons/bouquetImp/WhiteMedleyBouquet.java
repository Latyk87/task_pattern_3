package com.epam.model.flowersalons.bouquetImp;

import com.epam.model.flowersalons.Bouquet;
import com.epam.view.Composition;
import java.util.LinkedList;
import java.util.List;
/**
 * Class  describes White Medley bouquet in decorator pattern.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class WhiteMedleyBouquet implements Bouquet, Composition {
    final private double PRICE=80.0;
    final private String NAME="White Medley Bouquet";
    private List<String> composition;

    public WhiteMedleyBouquet(){
        composition=new LinkedList<>();
        composition.add("3 x White Spray Stocks");
        composition.add("7 x White Rose");
        composition.add("5 x White Double Lisianthus");
        composition.add("5 x White Trachelium");
        composition.add("5 x Eucalyptus");

    }

    @Override
    public double getCosts() {
        return PRICE;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public List<String> getFlowersAndRibonns() {
        return composition;
    }
}
