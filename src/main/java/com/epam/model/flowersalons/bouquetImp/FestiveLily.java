package com.epam.model.flowersalons.bouquetImp;

import com.epam.model.flowersalons.Bouquet;
import com.epam.view.Composition;
import java.util.LinkedList;
import java.util.List;
/**
 * Class  describes Festive Lily in decorator pattern.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class FestiveLily implements Bouquet, Composition {
    final private double PRICE=45.0;
    final private String NAME="Festive Lily";
    private List<String> composition;

    public FestiveLily(){
        composition=new LinkedList<>();
        composition.add("5 x White Oriental Lilies");
        composition.add("3 x Scots Pine");
        composition.add("4 x Laurel");
        composition.add("2 x Gold Painted Kentia Palm");

    }

    @Override
    public double getCosts() {
        return PRICE;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public List<String> getFlowersAndRibonns() {
        return composition;
    }
}
