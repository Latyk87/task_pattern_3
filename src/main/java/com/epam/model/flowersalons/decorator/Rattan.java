package com.epam.model.flowersalons.decorator;
/**
 * Class describes additional decoration Rattan in decorator pattern.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class Rattan extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = 12.0;
    private final String ADDITIONAL_COMPONENNT = "added Rattan";

    public Rattan() {
        setAdditionalPrice(ADDITIONAL_PRICE);
        setAdditionalComponent(ADDITIONAL_COMPONENNT);
    }
}
