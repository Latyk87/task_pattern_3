package com.epam.model.flowersalons.decorator;
/**
 * Class describes additional decoration Gift bag in decorator pattern.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class GiftBag extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = 25.0;
    private final String ADDITIONAL_COMPONENNT = "added Gift Bag";

    public GiftBag() {
        setAdditionalPrice(ADDITIONAL_PRICE);
        setAdditionalComponent(ADDITIONAL_COMPONENNT);
    }
}
