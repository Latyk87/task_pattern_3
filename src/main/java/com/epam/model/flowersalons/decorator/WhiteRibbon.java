package com.epam.model.flowersalons.decorator;
/**
 * Class describes additional decoration White ribbon in decorator pattern.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class WhiteRibbon extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = 10.0;
    private final String ADDITIONAL_COMPONENNT = "added White Ribbon";

    public WhiteRibbon() {
        setAdditionalPrice(ADDITIONAL_PRICE);
        setAdditionalComponent(ADDITIONAL_COMPONENNT);
    }
}
