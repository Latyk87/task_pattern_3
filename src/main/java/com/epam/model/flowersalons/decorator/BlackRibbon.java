package com.epam.model.flowersalons.decorator;
/**
 * Class describes additional decoration Black ribbon in decorator pattern.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class BlackRibbon extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = 10.0;
    private final String ADDITIONAL_COMPONENNT = "added Black Ribbon";

    public BlackRibbon() {
        setAdditionalPrice(ADDITIONAL_PRICE);
        setAdditionalComponent(ADDITIONAL_COMPONENNT);
    }


}
