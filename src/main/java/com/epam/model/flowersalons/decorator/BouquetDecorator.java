package com.epam.model.flowersalons.decorator;

import com.epam.model.flowersalons.Bouquet;
import java.util.List;
import java.util.Optional;

/**
 * Class contains the main logic of decorator pattern.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class BouquetDecorator implements Bouquet {

    private Optional<Bouquet> bouquetOptional;
    public static Double i = 0.0;
    private Double additionalPrice;
    private String toName = "";
    private String additionalComponent;

    public void setBouquetOptional(Bouquet bouquetOptionalOut) {
        bouquetOptional = Optional.ofNullable(bouquetOptionalOut);
        if (additionalComponent != null) {
            bouquetOptional.orElseThrow(IllegalArgumentException::new).getFlowersAndRibonns().add(additionalComponent);
        }
    }

    public void setAdditionalPrice(double additionalPrice) {
        this.additionalPrice = additionalPrice;
        i += additionalPrice;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public void setAdditionalComponent(String additionalComponent) {
        this.additionalComponent = additionalComponent;
    }


    @Override
    public double getCosts() {
        return bouquetOptional.orElseThrow(IllegalArgumentException::new).getCosts() + additionalPrice;
    }

    @Override
    public String getName() {
        return bouquetOptional.orElseThrow(IllegalArgumentException::new).getName() + toName;
    }

    @Override
    public List<String> getFlowersAndRibonns() {
        return bouquetOptional.orElseThrow(IllegalArgumentException::new).getFlowersAndRibonns();
    }
}
