package com.epam.model.flowersalons.decorator;
/**
 * Class describes additional decoration Correlative in decorator pattern.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class Correlative extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = 20.0;
    private final String ADDITIONAL_COMPONENNT = "added Correlative";

    public Correlative() {
        setAdditionalPrice(ADDITIONAL_PRICE);
        setAdditionalComponent(ADDITIONAL_COMPONENNT);
    }


}
