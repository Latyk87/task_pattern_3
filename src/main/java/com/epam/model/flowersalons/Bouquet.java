package com.epam.model.flowersalons;

import java.util.List;

/**
 * Interface to implement decorations of bouquets.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public interface Bouquet {
    double getCosts();

    String getName();

    List<String> getFlowersAndRibonns();

}
