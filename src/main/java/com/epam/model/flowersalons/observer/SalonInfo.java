package com.epam.model.flowersalons.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Class implements observer pattern logic.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class SalonInfo {

    private String info;
    private List<Information> links = new ArrayList<>();

    public void addObserver(Information information) {
        this.links.add(information);
    }

    public void removeObserver(Information information) {
        this.links.remove(information);
    }

    public void setInfo(String info) {
        this.info = info;
        for (Information i : this.links) {
            i.update(this.info);

        }
    }

}


