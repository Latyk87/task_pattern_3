package com.epam.model.flowersalons.observer;

/**
 * Interface to implement feedback to the client.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public interface Information {
 void update(Object o);
}
