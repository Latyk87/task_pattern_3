package com.epam.model.flowersalons.observer;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class implements observer pattern logic.
 * Created by Borys Latyk on 24/12/2019.
 *
 * @version 2.1
 * @since 24.12.2019
 */
public class ClientsInfo implements Information {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private String info;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public void update(Object info) {
        this.setInfo((String) info);
        logger1.info(this.info);
    }

    @Override
    public String toString() {
        return "ClientsInfo{" +
                "info='" + info + '\'' +
                '}';
    }
}
